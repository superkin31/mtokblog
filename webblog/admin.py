from django.contrib import admin

from .models import Blog

class PostAdmin(admin.ModelAdmin):
    prepopulated_fields = {'post_slug': ('post_title',)}

admin.site.register(Blog,PostAdmin)
