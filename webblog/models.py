from django.db import models
from stdimage.models import StdImageField
from tinymce import models as tinymce_models
from django.contrib.auth.models import User 


class Blog(models.Model):
    POST_STATUS = (
            ('P', 'Published'),
            ('D', 'Drafted'),
            ('C', 'Closed'),
        )
    TYPE_OF_POST = (
            ('1', 'Entertainment'),
            ('2', 'Knowledge'),
            ('3', 'Sport'),
        )
    post_title = models.CharField(max_length=120)
    post_slug =  models.SlugField(max_length=120)
    post_feature_image = StdImageField(upload_to='') 
    post_excerpt = models.CharField(max_length=64)
    post_content = tinymce_models.HTMLField()
    post_status = models.CharField(max_length=64, choices=POST_STATUS)
    post_type = models.CharField(max_length=64, choices=TYPE_OF_POST)
    create_at = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)


    def __str__(self):
        return f"{self.post_title} [{self.post_status}]"
        




    
