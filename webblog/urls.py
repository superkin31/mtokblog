from django.urls import path

from . import views

urlpatterns = [
    path("",views.index, name="blog123"),
    path("blog",views.detail, name="index"),
    path("blog/<int:blog_id>", views.content, name="blogid")
]   