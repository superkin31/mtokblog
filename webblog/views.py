from django.shortcuts import render
from django.http import HttpResponse, Http404
from .models import Blog

def index(request):
    return render(request, "blogexample.html") 

def detail(request):
    context = {
        "blogs" : Blog.objects.all()[:12] 
    }
    return render(request, "blogs.html", context)

def content(request, blog_id):
    try:
        blog = Blog.objects.get(pk=blog_id)
    except Blog.DoesNotExist:
        raise Http404("Blog does not exist.")
    context = {
        "blog" : blog,
        "blogs" : Blog.objects.exclude(id = blog.id)[:3] 
    }
    return render(request, "blogid.html", context)

        
        

